<?php
/**
 * Created by Ivan Mikho.
 * Date: 16.06.16
 * Time: 13:54
 */

namespace app\commands;

use app\rbac\ResetPasswordRule;
use app\rbac\ResetRootPasswordRule;
use Yii;
use yii\console\Controller;
use \app\rbac\UserGroupRule;
use \app\rbac\UserProfileOwnerRule;
use \app\rbac\GrantRootPrivilegesRule;
use \app\rbac\ChangeStatusRule;
use \app\rbac\LoginRule;

class RbacController extends Controller
{
    public function actionInit()
    {
        $auth = Yii::$app->authManager;
        $auth->removeAll();

        /* ROLES */
        $guest = $auth->createRole('guest');
        $admin = $auth->createRole('admin');
        $root = $auth->createRole('root');
        
//        $userGroupRule = new UserGroupRule();
//        $auth->add($userGroupRule);
//
//        $guest->ruleName = $userGroupRule->name;
//        $admin->ruleName = $userGroupRule->name;
//        $root->ruleName = $userGroupRule->name;

        $auth->add($guest);
        $auth->add($admin);
        $auth->add($root);

        $userProfileOwnerRule = new UserProfileOwnerRule();
        $grantRootPrivilegesRule = new GrantRootPrivilegesRule();
        $changeStatusRule = new ChangeStatusRule();
        $loginRule = new LoginRule();
        $resetRootPasswordRule = new ResetRootPasswordRule();
        $resetPasswordRule = new ResetPasswordRule();

        $auth->add($userProfileOwnerRule);
        $auth->add($grantRootPrivilegesRule);
        $auth->add($changeStatusRule);
        $auth->add($loginRule);
        $auth->add($resetRootPasswordRule);
        $auth->add($resetPasswordRule);

        $updateOwnProfile = $auth->createPermission('updateOwnProfile');
        $updateOwnProfile->ruleName = $userProfileOwnerRule->name;
        $auth->add($updateOwnProfile);

        $grantRootPrivileges = $auth->createPermission('grantRootPrivileges');
        $grantRootPrivileges->ruleName = $grantRootPrivilegesRule->name;
        $auth->add($grantRootPrivileges);

        $changeStatus = $auth->createPermission('changeStatus');
        $changeStatus->ruleName = $changeStatusRule->name;
        $auth->add($changeStatus);

        $resetRootPassword = $auth->createPermission('reset-root-pswd');
        $resetRootPassword->ruleName = $resetRootPasswordRule->name;
        $auth->add($resetRootPassword);

        $resetPassword = $auth->createPermission('reset-pswd');
        $resetPassword->ruleName = $resetPasswordRule->name;
        $auth->add($resetPassword);

        $loginPerm = $auth->createPermission('login-perm');
        $loginPerm->ruleName = $loginRule->name;
        $auth->add($loginPerm);

        /* ROLE_GUEST */
        $auth->addChild($guest, $resetPassword);
        $auth->addChild($guest, $loginPerm);

        /* ROLE_ADMIN */
        $auth->addChild($admin, $guest);
        $auth->addChild($admin, $updateOwnProfile);

        /* ROLE_ROOT */
        $auth->addChild($root, $admin);
        $auth->addChild($root, $grantRootPrivileges);
        $auth->addChild($root, $changeStatus);
        $auth->addChild($root, $resetRootPassword);
    }
}