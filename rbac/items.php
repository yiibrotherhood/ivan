<?php
return [
    'login' => [
        'type' => 2,
    ],
    'contact' => [
        'type' => 2,
    ],
    'logout' => [
        'type' => 2,
    ],
    'create' => [
        'type' => 2,
    ],
    'error' => [
        'type' => 2,
    ],
    'signup' => [
        'type' => 2,
    ],
    'index' => [
        'type' => 2,
    ],
    'view' => [
        'type' => 2,
    ],
    'update' => [
        'type' => 2,
    ],
    'captcha' => [
        'type' => 2,
    ],
    'grant' => [
        'type' => 2,
    ],
    'status' => [
        'type' => 2,
    ],
    'guest' => [
        'type' => 1,
        'ruleName' => 'userGroup',
        'children' => [
            'contact',
            'logout',
            'error',
            'login',
            'signup',
            'index',
            'captcha',
        ],
    ],
    'admin' => [
        'type' => 1,
        'ruleName' => 'userGroup',
        'children' => [
            'update',
            'guest',
        ],
    ],
    'root' => [
        'type' => 1,
        'ruleName' => 'userGroup',
        'children' => [
            'view',
            'create',
            'status',
            'grant',
            'admin',
        ],
    ],
];
