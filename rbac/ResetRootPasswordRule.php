<?php
/**
 * Created by Ivan Mikho.
 * Date: 21.06.16
 * Time: 12:24
 */

namespace app\rbac;

use app\models\Administrator;
use yii\rbac\Item;
use yii\rbac\Rule;

class ResetRootPasswordRule extends Rule 
{
    public $name = 'resetRootPasswordRule';

    public function execute($user, $item, $params)
    {
        if (\Yii::$app->user->isGuest) {
            return false;
        }
        if ( isset($params['id']) and \Yii::$app->user->identity->hasRole(Administrator::ROLE_ROOT) and \Yii::$app->user->id != $params['id'] ) {
            if ( Administrator::findOne($params['id'])->hasRole(Administrator::ROLE_ROOT)) {
                return true;
            }            
        }
        return false;
    }
}