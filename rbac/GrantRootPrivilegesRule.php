<?php
/**
 * Created by Ivan Mikho.
 * Date: 20.06.16
 * Time: 9:10
 */


namespace app\rbac;

use Yii;
use app\models\Administrator;
use yii\rbac\Rule;

class GrantRootPrivilegesRule extends Rule
{
    public $name = 'grantRootPrivilegesRule';

    public function execute($user, $item, $params)
    {
        if(Yii::$app->user->identity->hasRole(Administrator::ROLE_ROOT) and isset($params['id'])) {
            if (Administrator::findOne($params['id'])->hasRole(Administrator::ROLE_ADMIN)) {
                return true;
            }
        }
        return false;
    }
}