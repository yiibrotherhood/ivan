<?php
/**
 * Created by Ivan Mikho.
 * Date: 16.06.16
 * Time: 14:23
 */

namespace app\rbac;

use Yii;
use yii\rbac\Rule;
use app\models\Administrator;

class UserGroupRule extends Rule
{
    public $name = 'userGroup';

    public function execute($user, $item, $params)
    {
        if (!Yii::$app->user->isGuest) {
            $role = Yii::$app->user->identity->role;
            if ($item->name == 'root') {
                return $role == Administrator::ROLE_ROOT;
            } elseif ($item->name == 'admin') {
                return $role == Administrator::ROLE_ADMIN || $role == Administrator::ROLE_ROOT;
            }
        }
        return true;
    }
}