<?php
/**
 * Created by Ivan Mikho.
 * Date: 21.06.16
 * Time: 12:38
 */

namespace app\rbac;

use app\models\Administrator;
use yii\rbac\Item;
use yii\rbac\Rule;

class ResetPasswordRule extends Rule
{
    public $name = 'resetPasswordRule';

    public function execute($user, $item, $params)
    {
        if ( isset($params['id']) ) {
            $admin = Administrator::findOne($params['id']);
            if ( $admin != null and $admin->status == Administrator::STATUS_ACTIVE and $admin->hasRole(Administrator::ROLE_ADMIN) ) {
                return true;
            }
        }
        return false;
    }


}