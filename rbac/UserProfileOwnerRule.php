<?php
/**
 * Created by Ivan Mikho.
 * Date: 17.06.16
 * Time: 9:46
 */

namespace app\rbac;

use app\models\Administrator;
use Yii;
use yii\rbac\Rule;
use yii\rbac\Item;

class UserProfileOwnerRule extends Rule
{
    public $name = 'isProfileOwner';

    public function execute($user, $item, $params)
    {
        if (Yii::$app->user->isGuest) {
            return false;
        }
        if (Yii::$app->user->identity->hasRole(Yii::$app->authManager->getRole('root'))) {
            return true;
        }        
        return isset($params['id']) ? Yii::$app->user->id == $params['id'] : false;
    }
}