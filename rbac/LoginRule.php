<?php
/**
 * Created by Ivan Mikho.
 * Date: 20.06.16
 * Time: 10:16
 */

namespace app\rbac;

use app\models\Administrator;
use Yii;
use yii\rbac\Rule;

class LoginRule extends Rule 
{
    public $name = 'isActiveRule';

    public function execute($user, $item, $params)
    {
        if (isset($params['username'])) {
            $user = Administrator::findOne(['email' => $params['username']]);
            if ($user != null and $user->status == Administrator::STATUS_ACTIVE) {
                return true;
            }
        }
        return false;
    }
}