<?php
/**
 * Created by Ivan Mikho.
 * Date: 15.06.16
 * Time: 14:47
 */
namespace app\models;

use Yii;
use yii\base\Model;
use app\models\Administrator;

class EmailConfirmation extends Model
{
    public $email;

    public function rules()
    {
        return [
            ['email', 'email'],
        ];
    }
    /**
     * @inheritdoc
     */
   public function validate($attributeNames = null, $clearErrors = true)
   {
       if (parent::validate($attributeNames, $clearErrors)) {
           $user = Administrator::findByUsername($this->email);
           if ($user != null and $user->hasRole(Administrator::ROLE_ADMIN) and $user->status == Administrator::STATUS_ACTIVE) {
               $user->password_reset_token_hash = Yii::$app->security->generateRandomString();
               $token = $user->password_reset_token_hash;
               if ($user->save()) {
                   return Yii::$app->mailer->compose(['html' => '@app/mail/reset-password'], ['user' => $user, 'token' => $token])
                       ->setFrom('akievtest@gmail.com')
                       ->setTo($user->email)
                       ->setSubject("Reset password")
                       ->send();                  
               }
               
           }
       } 
       return false;
   }
}