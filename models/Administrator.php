<?php

namespace app\models;

use Yii;
use yii\behaviors\SluggableBehavior;
use yii\rbac\Assignment;
use yii\rbac\Role;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "administrator".
 *
 * @property integer $id
 * @property string $name
 * @property string $surname
 * @property string $slug
 * @property integer $company_id
 * @property string $email
 * @property string $password_hash
 * @property string $auth_key
 * @property string $access_token
 * @property int $role
 * @property int $status
 * @property string $activation_token_hash
 * @property string $password_reset_token_hash
 */
class Administrator extends \yii\db\ActiveRecord implements IdentityInterface
{

    const ROLE_ADMIN = 10;
    const ROLE_ROOT = 20;
    
    const STATUS_BLOCK = 0;
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 2;
    
    public function behaviors()
    {
        return [
            'slug' => [
                'class' => SluggableBehavior::className(),
                'attribute' => ['name', 'surname'],
                'ensureUnique' => true,
            ],
        ];
    }


    public function beforeSave($insert)
    {
        //$this->company_id =$company->id;
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                $this->auth_key = Yii::$app->security->generateRandomString();
                $this->password_hash = Yii::$app->security->generatePasswordHash($this->password_hash);
                $this->access_token = Yii::$app->security->generateRandomString();
                $this->activation_token_hash = Yii::$app->security->generatePasswordHash($this->activation_token_hash);
                $this->password_reset_token_hash = Yii::$app->security->generatePasswordHash($this->password_reset_token_hash);
                $this->role = self::ROLE_ADMIN;
                $this->status = self::STATUS_INACTIVE;
            } else {
                if (self::findOne($this->id)->password_hash != $this->password_hash){
                    $this->password_hash = Yii::$app->security->generatePasswordHash($this->password_hash);
                }
                if (self::findOne($this->id)->password_reset_token_hash != $this->password_reset_token_hash){
                    $this->password_reset_token_hash = Yii::$app->security->generatePasswordHash($this->password_reset_token_hash);
                }
            }

            return true;
        }
        return false;
    }


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'administrator';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'surname', 'email', 'password_hash'], 'required'],
            [['email'], 'unique'],
            [['company_id'], 'integer'],
            [['name', 'surname', 'email'], 'trim'],
            [['name', 'surname', 'slug', 'email', 'password_hash', 'auth_key', 'access_token'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'surname' => 'Surname',
            'slug' => 'Slug',
            'company_id' => 'Company ID',
            'email' => 'Email',
            'password_hash' => 'Password',
            'auth_key' => 'Auth Key',
            'access_token' => 'Access Token',
        ];
    }

    public function assignRole($roleName)
    {
        $auth = Yii::$app->authManager;
        $adminRole = $auth->getRole($roleName);
        return $auth->assign($adminRole, $this->id) instanceof Assignment;

    }


    public function changePassword($pass)
    {
        if(Yii::$app->getSecurity()->validatePassword($pass->oldPass, $this->password_hash))
        {
            $this->password_hash = $pass->newPass;
            $this->save();
        }
    }

    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    public function grant()
    {
        $this->role = self::ROLE_ROOT;
        $this->assignRole('root');
        return $this->save();
    }
    
    public function block()
    {
        $this->status = self::STATUS_BLOCK;
        return $this->save();
    }

    public function active()
    {
        $this->status = self::STATUS_ACTIVE;
        return $this->save();
    }

    public function confirmEmail($token) {
        if ($this->status == Administrator::STATUS_INACTIVE and Yii::$app->security->validatePassword($token, $this->activation_token_hash)) {
            $this->status = Administrator::STATUS_ACTIVE;
            return $this->save();
        }
        return false;
    }

    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['access_token' => $token]);
    }

    public function getId()
    {
        return $this->id;
    }

    public function getAuthKey()
    {
        return $this->auth_key;
    }

    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    public function validatePassword($password)
    {
        $valid = Yii::$app->getSecurity()->validatePassword($password, $this->password_hash);
        return $valid;
    }

    public static function findByUsername($username)
    {
        return static::findOne(['email' => $username]);
    }
    
    public function hasRole($role)
    {
        if ($role instanceof Role) {
            $roles = Yii::$app->authManager->getRolesByUser($this->id);
            return in_array($role, $roles);
        } elseif (is_string($role)) {
            $role = Yii::$app->authManager->getRole($role);
            $roles = Yii::$app->authManager->getRolesByUser($this->id);
            return in_array($role, $roles);
        }
        return $this->role == self::ROLE_ROOT ? true : $role == $this->role;
    }

}
