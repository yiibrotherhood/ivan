<?php

namespace app\models;

use Yii;
use app\models\Company;

/**
 * This is the model class for table "employer".
 *
 * @property integer $id
 * @property string $name
 * @property string $surname
 * @property string $department
 * @property double $salary
 */
class Employer extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'employer';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'surname'], 'required'],
            [['salary'], 'number'],
            [['name', 'surname', 'department'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'surname' => 'Surname',
            'department' => 'Department',
            'salary' => 'Salary',
        ];
    }

    public function getCompanies()
    {
        return $this->hasMany(Company::className(),['id' => 'company_id'])
            ->viaTable('workIn', ['employer_id' => 'id']);
    }
}
