<?php
/**
 * Created by Ivan Mikho.
 * Date: 15.06.16
 * Time: 11:17
 */

namespace app\models;

use yii\base\Model;

class ChangePassword extends Model {
    public $oldPass;
    public $newPass;
    public $newPassEq;
}