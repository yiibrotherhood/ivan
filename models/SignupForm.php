<?php
/**
 * Created by Ivan Mikho.
 * Date: 15.06.16
 * Time: 14:47
 */
namespace app\models;

use Yii;
use yii\validators\FilterValidator;
use yii\base\Model;
use app\models\Administrator;
use app\models\CompanySearch;

class SignupForm extends Model
{
    public $name;
    public $surname;
    public $email;
    public $password;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'surname', 'email'], 'trim'],
            [['name', 'surname', 'email', 'password'], 'required'],
            ['email', 'email'],
            [['name', 'surname', 'email'], 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\app\models\Administrator', 'message' => 'This email address has already been taken.'],
            ['password', 'string', 'min' => 6],
        ];
    }

    public function sendActivationEmail($user, $token)
    {
        return Yii::$app->mailer->compose(['html' => '@app/mail/activation'], ['user' => $user, 'token' => $token])
            ->setFrom('akievtest@gmail.com')
            ->setTo($user->email)
            ->setSubject("Confirm registration")
            ->send();
    }
    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }
        
        $admin = new Administrator();
        $admin->name = $this->name;
        $admin->surname = $this->surname;
        $admin->email = $this->email;
        $admin->password_hash = $this->password;
        $admin->company_id = array_rand(CompanySearch::getCompaniesArray());
        $admin->activation_token_hash = Yii::$app->security->generateRandomString();
        $activation_token_hash = $admin->activation_token_hash;
        
        return $admin->save() and $this->sendActivationEmail($admin, $activation_token_hash) and $admin->assignRole('admin');
    }
}