<?php
use yii\helpers\Html;
/* @var $this yii\web\View */
/* @var $user app\models\Administrator */
/* @var $token string */
$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['administrator/reset-password', 'token' => $token, 'id' => $user->id]);
?>
<div class="password-reset">
    <p>Hello <?= Html::encode($user->name) ?>,</p>

    <p>Follow the link below to reset your password:</p>

    <p><?= Html::a(Html::encode(">> reset password <<"), $resetLink) ?></p>
</div>