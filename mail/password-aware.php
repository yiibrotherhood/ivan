<?php
use yii\helpers\Html;
/* @var $this yii\web\View */
/* @var $user app\models\Administrator */
/* @var $token string */
?>
<div class="password-aware">
    <p>Hello <?= Html::encode($user->name) ?>,</p>

    <p>Your password changed by <?= Html::encode($root->name) ?>, please write him to know more: <?= Html::encode($root->email) ?>.</p>
    <p>Do not send password via email.</p>


</div>