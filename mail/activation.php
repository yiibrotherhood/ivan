<?php
use yii\helpers\Html;
/* @var $this yii\web\View */
/* @var $user app\models\Administrator */
/* @car $token string */
$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['administrator/confirm-email', 'id' => $user->id, 'token' => $token]);

?>
<div class="password-reset">
    <p>Hello <?= Html::encode($user->name) ?>,</p>

    <p>Please complete registration with confirm your email by clicking <?= Html::a(Html::encode('link'), $resetLink) ?></p>
</div>