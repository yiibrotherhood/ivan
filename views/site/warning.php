<?php
/**
 * Created by Ivan Mikho.
 * Date: 20.06.16
 * Time: 16:37
 */

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

?>
<div class="site-warning">
</div>
<div class="col-sm-6 col-lg-offset-3">
    <div class="panel panel-warning">
        <div class="panel-heading">
            <h4>Warning!</h4>
        </div>
        <div class="panel-body">
            <?= nl2br(Html::encode($message)) ?>
        </div>
    </div>
</div>