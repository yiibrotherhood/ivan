<?php
/**
 * Created by Ivan Mikho.
 * Date: 15.06.16
 * Time: 15:07
 */

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\EmailConfirmation */
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
$this->title = 'Confirm email';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-signup">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>Please fill out your email:</p>

    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(['id' => 'email-confirm']); ?>

            <?= $form->field($model, 'email')->textInput(['autofocus' => true]) ?>

            <div class="form-group">
                <?= Html::submitButton('Confirm', ['class' => 'btn btn-primary', 'name' => 'confirm-button']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>