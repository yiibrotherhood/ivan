<?php
/**
 * Created by Ivan Mikho.
 * Date: 15.06.16
 * Time: 15:07
 */

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ResetPassword */
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
$this->title = 'Reset password';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-signup">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>Please create a new password:</p>

    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(['id' => 'reset-password']); ?>

            <?= $form->field($model, 'password')->passwordInput() ?>

            <?= $form->field($model, 'passwordRpt')->passwordInput() ?>

            <div class="form-group">
                <?= Html::submitButton('Reset', ['class' => 'btn btn-primary', 'name' => 'reset-button']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>