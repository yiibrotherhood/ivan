<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Administrator */
/* @var $company app\models\Company */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Administrators', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="administrator-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        <?php if (!Yii::$app->user->isGuest and Yii::$app->user->identity->hasRole('root') and !$model->hasRole('root')) {?>
            <?= Html::a('Grant root acces', ['grant', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a((($model->status == \app\models\Administrator::STATUS_BLOCK) ? "Active admin" : "Block admin"), ['status', 'id' => $model->id, 'status' => $model->status], ['class' => 'btn btn-primary']) ?>
        <?php } ?>
        <?php if (!Yii::$app->user->isGuest and Yii::$app->user->identity->hasRole('root') and $model->hasRole('root') and $model->id != Yii::$app->user->id) {?>
            <?= Html::a('Reset password', ['reset-root-password', 'id' => $model->id], ['class' => 'btn btn-warning']) ?>
        <?php } ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'surname',
            'slug',
            'company_id',
            'email:email',
        ],
    ]) ?>
    
    <h2>Company: </h2>
    <?= DetailView::widget([
        'model' => $company,
        'attributes' => [
            'id',
            'name',
        ],
    ]) ?>

</div>
