<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Administrator */
/* @var $companies [] */
/* @var $form yii\widgets\ActiveForm */
/* @var $pass app\models\ChangePassword*/
?>

<div class="administrator-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'surname')->textInput(['maxlength' => true]) ?>

    
    <?php if (Yii::$app->user->identity->hasRole('root')) { ?>
    <?= $form->field($model, 'company_id')->dropDownList($companies) ?>
    <?php } ?>


    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?php if ($model->isNewRecord) {?>

        <?= $form->field($model, 'password_hash')->passwordInput(['maxlength' => true]) ?>

    <?php } else { ?>

        <?= $form->field($pass, 'oldPass')->passwordInput(); ?>

        <?= $form->field($pass, 'newPass')->passwordInput(); ?>

        <?= $form->field($pass, 'newPassEq')->passwordInput(); ?>

        <?= Html::a('Change Password', ['/administrator/change-password', 'id' => $model->id, 'pass' => json_encode($pass)], ['class' => 'btn btn-default', 'on-click' => '$.ajax("/administrator/change-password",{data: <?php echo json_encode($pass)?>})']) ?>

    <?php } ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
