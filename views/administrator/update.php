<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Administrator */
/* @var $companies app\models\Company[] */
/* @var $pass app\models\ChangePassword */

$this->title = 'Update Administrator: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Administrators', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="administrator-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'companies' => $companies,
        'pass' => $pass,
    ]) ?>

</div>
