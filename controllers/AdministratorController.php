<?php

namespace app\controllers;

use app\models\EmailConfirmation;
use app\models\LoginForm;
use app\models\ResetPassword;
use Yii;
use app\models\Administrator;
use app\models\AdministratorSearch;
use app\models\CompanySearch;
use app\models\ChangePassword;
use app\models\SignupForm;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\data\Pagination;

/**
 * AdministratorController implements the CRUD actions for Administrator model.
 */
class AdministratorController extends Controller
{
    /**
     * @inheritdoc
     */

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['logout', 'index', 'update', 'view'],
                        'roles' => ['admin'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['grant', 'reset-root-password', 'status', 'delete'],
                        'roles' => ['root'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['signup', 'reset-password', 'confirm-email', 'request-reset-password'],
                        'roles' => ['?'],
                    ],
                ]
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Administrator models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AdministratorSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'pagination' => $dataProvider->pagination,
        ]);
    }

    /**
     * Displays a single Administrator model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id = null, $slug = null)
    {
        $admin = $this->findModel($id, $slug);
        return $this->render('view', [
            'model' => $admin,
            'company' => $admin->company,
        ]);                
    }
    
    /**
     * Creates a new Administrator model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Administrator();
        $companies = CompanySearch::getCompaniesArray();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->user->login($model);
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'companies' => $companies,
            ]);
        }
    }

    /**
     * Updates an existing Administrator model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id = null, $slug = null)
    {
        if (!\Yii::$app->user->can('updateOwnProfile', ['id' => $id])) {
            throw new ForbiddenHttpException('Access denied');
        }

        $model = $this->findModel($id, $slug);
        $company = $model->company_id;
        $companies = CompanySearch::getCompaniesArray();
        $pass = new ChangePassword();
        if ($model->load(Yii::$app->request->post()) && (Yii::$app->user->identity->hasRole('root') or $model->company_id == $company)) {
            if ($model->save()){
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            if ($model->company_id != $company){
                throw new ForbiddenHttpException("You can not change you company!");
                return false;
            }
            return $this->render('update', [
                'model' => $model,
                'companies' => $companies,
                'pass' => $pass,
            ]);
        }


    }

    public function actionChangePassword($id, $slug = null, $pass)
    {
        $pass = json_decode($pass);
        $model = $this->findModel($id, $slug);
        $model->changePassword($pass);
    }

    /**
     * Deletes an existing Administrator model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id = null, $slug = null)
    {
            $this->findModel($id, $slug)->delete();
            return $this->redirect(['index']);
    }

    public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($model->signup()) {
                return $this->render('success', ['message' => 'Your account created successfully! To complete registration confirm your email. Confirmation link sent to your email ' . $model->email]);
            }
        }
        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    public function actionGrant($id = null, $slug = null) {
        $admin = $this->findModel($id, $slug);
        if (Yii::$app->user->can('grantRootPrivileges', ['id' => $admin->id])) {
            $admin->grant();
            return $this->redirect(['view', 'id' => $id]);
        }
        throw new ForbiddenHttpException;
    }

    public function actionStatus($id = null, $slug = null, $status) {
        $admin = $this->findModel($id, $slug);
        if(Yii::$app->user->can('changeStatus', ['id' => $admin->id])) {
            if ($status == Administrator::STATUS_BLOCK) {
                $admin->active();
            }
            if ($status == Administrator::STATUS_ACTIVE) {
                $admin->block();
            }

            return $this->redirect(['view', 'id' => $id]);
        }
        throw new ForbiddenHttpException;
    }

    public function actionConfirmEmail($id = null, $slug = null, $token)
    {
        $user = $this->findModel($id, $slug);
        if ($user->confirmEmail($token)) {
            return $this->render('success', ['message' => 'Your account is successfully activated']);
        }
    }
    
    public function actionRequestResetPassword() 
    {
        $model = new EmailConfirmation();
        if ($model->load(Yii::$app->request->post()) and $model->validate()) {
            return $this->render('success', ['message' => 'Email with reset link sent to your email']);
        } else {
            return $this->render('email-confirm', [
                'model' => $model,
            ]);
        }
    }    
    
    public function actionResetPassword($id, $token) {
        $admin = $this->findModel($id, null);
        if (Yii::$app->security->validatePassword($token, $admin->password_reset_token_hash) and Yii::$app->user->can('reset-pswd', ['id' => $admin->id])) {
            $model = new ResetPassword();
            if ($model->load(Yii::$app->request->post()) and $model->reset($admin->id)) {
                return $this->render('success', ['message' => 'Password successfully changed']);
            } else {
                return $this->render('reset-password', [
                    'model' => $model,
                ]);
            }
        }
        throw new ForbiddenHttpException("Wrong reset password token");
    }

    public function actionResetRootPassword($id) {
        if ( Yii::$app->user->can('reset-root-pswd', ['id' => $id]) ){
            $model = new ResetPassword();
            if ($model->load(Yii::$app->request->post()) and $model->reset($id)) {
                return $this->render('success', ['message' => 'Password successfully changed']);
            } else {
                return $this->render('reset-password', [
                    'model' => $model,
                ]);
            }
        }
        throw new ForbiddenHttpException("Access denied");
    }


    /**
     * Finds the Administrator model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Administrator the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id, $slug)
    {
        if ($id !== null) {
            if (($model = Administrator::findOne($id)) !== null) {
                return $model;
            } else {
                throw new NotFoundHttpException('The requested page does not exist.');
            }
        }
        if ($slug !== null) {
            if (($model = Administrator::findOne(['slug' => $slug])) !== null) {
                return $model;
            } else {
                throw new NotFoundHttpException('The requested page does not exist.');
            }
        }
        throw new NotFoundHttpException('The requested page does not exist.');        
    }
}
